

import React from 'react';
import { StyleSheet, View } from 'react-native';
import Navigator from './config/navigator';
// import { Toast } from 'react-native-redux-toast';
import Constants from './constants';


export default class Root extends React.Component{
	render() {
		return (
			<View style={styles.container}>
				<Navigator />
				{/* <Toast messageStyle={styles.toastStyle} /> */}
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: Constants.Colors.Red
	},
	// toastStyle: {
	// 	color: Constants.Colors.White,
	// 	...Constants.Fonts.normal
	// }
});
