

import { AsyncStorage } from 'react-native';
import { persistCombineReducers } from 'redux-persist';
import { toastReducer as toast } from 'react-native-redux-toast';
import app from './app';
// import nav from './nav';
import user from './user';

const config = {
	key: 'primary',
	storage: AsyncStorage,
	blacklist: ['app', 'toast']
};

export default function getRootReducer() {
	return persistCombineReducers(config, {
		toast,
		app,
		user,
	});
}
