/*
 * @file: index.js
 * @description: Application Constants
 * @date: 04.June.2018
 * @author: Manish Budhiraja
 * */

export const HIDE_LOADER  = 'HIDE_LOADER';
export const SHOW_LOADER  = 'SHOW_LOADER';