
import { LOGIN, LOGOUT, UPDATE_PROFILE, DEVICE_TOKEN,CONTACTS } from './constants';


const initialState = {
  deviceToken: 'test',
  userDetails: null,
  contactDetails:null
};

export default function reducer(state = initialState, action) {
  switch (action.type) {

    case DEVICE_TOKEN:
    return {...state, deviceToken:action.payload};

    case LOGIN:
      return { ...state, isLoggedIn: true, userDetails: action.payload };

    case UPDATE_PROFILE:
      return { ...state, userDetails: { ...state.userDetails, ...action.payload } };

    case LOGOUT:
      return initialState;

  case CONTACTS:
      return {
        contactDetails: action.payload
      };
    default:
      return state;
  }
}
