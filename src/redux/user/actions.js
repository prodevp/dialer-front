
import { LOGIN, LOGOUT, UPDATE_PROFILE, DEVICE_TOKEN, CONTACTS } from './constants';
import axios from 'axios';
export const login = payload => ({ type: LOGIN, payload }); // login action.
export const logout = () => ({ type: LOGOUT }); // logout action to update reducer.
export const updateProfile = payload => ({ type: UPDATE_PROFILE, payload }); // logout action to update user profile.
export const setDeviceToken = payload => ({ type: DEVICE_TOKEN, payload }); // logout action to update user profile.
export const contactDetails = payload => ({ type: CONTACTS, payload });

/**
 *  APIs used in user
 */
export const contactDetailsComplete = (data) => {
    return dispatch => {
        return axios.get('https://dialer-backend.herokuapp.com/getcontacts')
            .then((response) => {
                console.log('response',response);
                return dispatch(contactDetails(response.data),
                );
            }
            )
            .catch((error) => {
                console.log(error, 'ghsdhjfk');

            });

    };
};