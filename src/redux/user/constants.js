

// Actions for reducer
export const LOGIN = 'LOGIN';
export const LOGOUT = 'LOGOUT';
export const UPDATE_PROFILE = 'UPDATE_PROFILE';
export const DEVICE_TOKEN = 'DEVICE_TOKEN';
export const CONTACTS = 'CONTACTS';