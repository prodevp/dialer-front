/*
 * @file: navUtility.js
 * @description: Configure navigation utility.
 * @date: 04.June.2018
 * @author: Manish Budhiraja
 * */

import {
  createReactNavigationReduxMiddleware,
  createNavigationPropConstructor,
} from 'react-navigation-redux-helpers';

const middleware = createReactNavigationReduxMiddleware(
  'root',
  state => state.nav
);
const navigationPropConstructor = createNavigationPropConstructor('root');

export { middleware, navigationPropConstructor };