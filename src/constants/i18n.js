
let Strings = {
	networkError: 'Please check your internet connectivity or our server is not responding.',
	common: {
		lorem:
			'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.'
	},
	noRecords: {
		comingSoon: 'Wish we were Here!',
		noResults: 'Sorry! There are no results to display.',
		noDelivery: 'We don\'t deliver here yet. Why don\'t you try a different location.'
	},
	permissionsErrors: {
		camera: 'Cannot access camera. Please allow access if you want to be able to click images.',
		gallery: 'Cannot access images. Please allow access if you want to be able to select images.'
	}
};

module.exports = Strings;
