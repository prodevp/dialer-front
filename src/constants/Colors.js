

let colors = {
	NavigationColor: 'rgba(235,235,235,0.7)',
	TabBarColor: '#c61b29',
	ButtonColor: '#c61b29',
	PrimaryColor: '#c61b29',
	AccentColor: '#be1523',

	Green: '#3d8c03',
	LightGreen: '#7cce6e',
	SearchTabBar:'#4DB6AC',
	Gray: '#737373',
	DarkGrey: '#545353',
	BlurGrey: 'rgba(115,115,115,0.4)',
	LightGray: '#a5a6a6',
	GrayishWhite: '#E8E8E8',
	SettingLightGray: '#f2f2f2',
	borderGray: '#c4c4c4',
    drawerBackground:'#2d324f',
	Black: '#181818',
	PureBlack: '#000000',
	LightBlack: '#575758',
    drawerText:'#0691ce',
	White: '#ffffff',
	WhiteBlur: 'rgba(235,235,235,0.7)',
	WhiteSmoke: '#f4f5f7',
	Snow: '#f2f3f6',
	GhostWhite: '#ebebeb',
	SmokeWhite: '#F5F5F5',
    menuContainer:'#11142a',
	Blue: '#031454',
	Orange: '#f68d57',
	Red: '#be1523',
	LightYellow: '#fff4d5',
	LightGrey: '#f7f6f6',

	Magenta: '#ef3881',
	Purple: '#a2a1b8',
	LightBrown: '#938d8a',

	Transparent: 'transparent',
	Translucent: 'rgba(0,0,0,.4)',
	HyperLink: '#0000EE'
};

module.exports = colors;
