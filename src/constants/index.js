
module.exports = {
	BaseStyle: require('./BaseStyle'),
	Colors: require('./Colors'),
	// Fonts: require('./Fonts'),
	i18n: require('./i18n'),
	Images: require('./Images'),
	ResendOTPTime: 59 // seconds
};
