
import React from 'react';
import { View, StyleSheet, Text, Image, TouchableOpacity, FlatList } from 'react-native';
import Constants from '../../constants';
import NavigationBar from 'react-native-navbar';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialIcons';
import * as contactDetailActions from '../../redux/user/actions';
import SpinnerWait from './Spinner';
class Loader extends React.Component {

	static navigationOptions = {
		header: null,
	}

	componentWillMount(){
		this.props.contactDetailsComplete();
	}
	_renderRow(item) {
		return (
			<View style={styles.mainList}>
				<View style={styles.photoContainer}>
					<Image source={{ uri: item.image }} style={styles.userImageStyle} />
				</View>
				<View style={styles.phoneContainer}>
				<Text style={styles.nameText}>{item.name}</Text>
				<Text style ={styles.phoneText}>{item.phoneNumber}</Text>
				</View>
				<View style={styles.iconContainer}>
				<TouchableOpacity style={styles.drawerStyle} activeOpacity={0.9} onPress={() => this.props.navigation.openDrawer()}><MaterialCommunityIcons name="list" size={30} color="grey" /></TouchableOpacity>
				</View>
			</View>
		);
	}
	render() {
		const titleConfig = { title: 'Contacts', tintColor: Constants.Colors.White, style: { fontSize: 18 } };
		return (
			<View style={styles.container}>
				<NavigationBar
					style={{ backgroundColor: Constants.Colors.SearchTabBar }}
					  title={titleConfig}
					leftButton={<TouchableOpacity style={styles.drawerStyle} activeOpacity={0.9}><MaterialCommunityIcons name="menu" size={30} color="#fff" /></TouchableOpacity>}
					rightButton={<TouchableOpacity style={styles.drawerStyle} activeOpacity={0.9} onPress={() => this.props.navigation.navigate('Profile',{data:this.props.user.contactDetails.length>0?this.props.user.contactDetails:[]})}><MaterialCommunityIcons name="search" size={30} color="#fff" /></TouchableOpacity>}
					statusBar={{ style: 'light-content', tintColor: Constants.Colors.Black }}
				/>
				<View >
				{this.props.user&& this.props.user.contactDetails === null?
				<SpinnerWait/>:
					<FlatList
					data={this.props.user.contactDetails.length>0?this.props.user.contactDetails:[]}
						showsVerticalScrollIndicator={false}
						renderItem={({ item }) =>
							this._renderRow(item)
						}
					/>
				}
				</View>
			</View>
		);
	}
}
const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: Constants.Colors.White
	},
	drawerStyle: {
		justifyContent: 'center',
		paddingHorizontal: Constants.BaseStyle.DEVICE_WIDTH / 100 * 0.5
	},
	mainList: {
		flexDirection: 'row',
		marginHorizontal:Constants.BaseStyle.DEVICE_WIDTH/100*3
	},
	photoContainer: {
		marginVertical: Constants.BaseStyle.DEVICE_WIDTH / 100 * 2,
		flex: 1
	},
	userImageStyle: {
		height: Constants.BaseStyle.DEVICE_WIDTH / 100 * 16,
		width: Constants.BaseStyle.DEVICE_WIDTH / 100 * 16,
		borderRadius: Constants.BaseStyle.DEVICE_WIDTH / 100 * 8
	},
	phoneContainer:{
		flex: 4,
		justifyContent:'center',
		alignContent:'center',
		marginLeft:Constants.BaseStyle.DEVICE_WIDTH/100*2
	},
	iconContainer:{
	alignContent:'flex-end',
	marginTop:Constants.BaseStyle.DEVICE_WIDTH/100*3
	},
	nameText:{
    fontSize:16
	},
	phoneText:{
		fontSize:14,
		color:Constants.Colors.SearchTabBar
	}
});
const mapDispatchToProps = (dispatch) => {
	return bindActionCreators({
	  ...contactDetailActions
	}, dispatch);
  };
  const mapStateToProps = (state) => state;
  export default connect(mapStateToProps, mapDispatchToProps)(Loader);

