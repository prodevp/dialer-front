

import React from 'react';
import Spinner from 'react-native-loading-spinner-overlay';




class SpinnerWait extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			visible: true
		};
	}

	componentDidMount() {
		let context = this;
		 setTimeout(() => {
			context.setState({
				visible: false
			});
		}, 3000);
	}

	render() {
		return <Spinner visible={this.state.visible} />;
	}
}

 export default SpinnerWait;