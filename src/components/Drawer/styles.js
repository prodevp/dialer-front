import { StyleSheet, Platform } from 'react-native';
import Constants from '../../constants';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    ...Platform.select({
      ios: {
        backgroundColor: '#11142a',
      },
      android: {
        backgroundColor: '#C5B9C9',
      }
    })
  },

  drawercontainer: {
    ...Platform.select({
      ios: {
        width: Constants.BaseStyle.DEVICE_WIDTH / 100 * 64,
      },
      android: {
        width: Constants.BaseStyle.DEVICE_WIDTH,
      }
    }),
    height: Constants.BaseStyle.DEVICE_HEIGHT,
    backgroundColor: Constants.Colors.drawerBackground,
  },
  headerSec: {
    backgroundColor: Constants.Colors.Transparent,
    height: Constants.BaseStyle.DEVICE_WIDTH * 0.15,
    borderBottomWidth: 0,
    ...Platform.select({
      ios: {},
      android: {
        marginTop:25
      }
    }),
    elevation: 0
  },

  left: {
    flex: 0.5,
    backgroundColor: Constants.Colors.Transparent,
  },
  backArrow: {
    width: 30,
    justifyContent: 'center',
    alignItems: 'center'
  },
  body: {
    flex: 3,
    alignItems: 'center',
    backgroundColor: Constants.Colors.Transparent
  },
  textTitle: {

  },
  right: {
    flex: 0.5
  },
  mainText: {
    color:Constants.Colors.drawerText,
    fontSize:17,
    height: (Constants.BaseStyle.DEVICE_HEIGHT * 0.05),
    width: (Constants.BaseStyle.DEVICE_WIDTH),
    alignItems: 'center',
    alignSelf: 'center',
    justifyContent: 'center',
    textAlign: 'center',
    backgroundColor: Constants.Colors.Transparent,
    top: (Constants.BaseStyle.DEVICE_HEIGHT * 0.44),
    // fontFamily: Fonts.type.sfuiDisplayRegular,
  },



  /*Menu Section START*/
  menuContainer: {
    backgroundColor:Constants.Colors.menuContainer ,
    ...Platform.select({
      ios: {
        width: Constants.BaseStyle.DEVICE_WIDTH / 100 * 64,
      },
      android: {
        width: Constants.BaseStyle.DEVICE_WIDTH,
      }
    }),

    height: Constants.BaseStyle.DEVICE_HEIGHT,
    paddingTop: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 5,
  },
  menucontrolPanel: {
    flex: 1,
  },
  userProfiles: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginLeft: Constants.BaseStyle.DEVICE_WIDTH / 100 * 4,
    marginTop: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 2,
  },
  userImageStyle: {
    width: (Constants.BaseStyle.DEVICE_WIDTH) * 0.20,
    height: (Constants.BaseStyle.DEVICE_WIDTH) * 0.20,
    borderRadius: (Constants.BaseStyle.DEVICE_WIDTH) * 0.10,
    borderWidth: 2,
    borderColor: Constants.Colors.White,
  },
  userDetailsStyle: {
    backgroundColor: Constants.Colors.Transparent,
    alignItems: 'flex-start',
    justifyContent: 'center',
    marginLeft: Constants.BaseStyle.DEVICE_WIDTH / 100 * 3
  },
  userDetailsText: {
    fontSize:18,
    color: Constants.Colors.White,
    // fontFamily: Fonts.type.sfuiDisplayMedium,
  },
  addressDetailText: {
    fontSize:16,
    color: Constants.Colors.White,
    // fontFamily: Fonts.type.sfuiDisplayMedium,
  },
  menumainview: {
    marginLeft: Constants.BaseStyle.DEVICE_WIDTH/100*5,
    marginTop: Constants.BaseStyle.DEVICE_HEIGHT/100*5,
  },
  listrow: {
    backgroundColor: Constants.Colors.Transparent,
    flexDirection: 'row',
    marginBottom: 15,
  },
  rowtxt: {
    color: Constants.Colors.White,
    fontSize:18,
    backgroundColor: Constants.Colors.Transparent,
    marginLeft: Constants.BaseStyle.DEVICE_WIDTH/100*5,
    textAlign: 'center',
    // fontFamily: Fonts.type.sfuiDisplayMedium,
  },
});
export default styles;