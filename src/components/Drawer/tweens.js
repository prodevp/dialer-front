const deviceScreen = require('Dimensions').get('window');

module.exports = {
  material: (ratio) => {
    let drawerShadow = ratio < .2 ? ratio*5*5 : 5;
    return {
      drawer: {
        shadowRadius: drawerShadow,
      },
      main: {
        opacity:(2-ratio)/2,
      },
    };
  },
  rotate: (ratio) => {
    let r0 = -ratio/8;
    let r1 = 1-ratio/2;
    let t = [
               r1,  r0,  0,  0,
               -r0, r1,  0,  0,
               0,   0,   1,  0,
               0,   0,   0,  1,
            ];

    return {
      main: {
        transformMatrix:t,
        left: ratio*300,
      },
    };
  },
  parallax: (ratio) => {
    let r1 = 1;
    let t = [
               r1,  0,  0,  0,
               0, r1,  0,  0,
               0,   0,   1,  0,
               0,   0,   0,  1,
            ];
    return {
      main: {
        left:deviceScreen.width*ratio/2,
        transformMatrix: t,
        opacity: 1-ratio*.3
      },
    };
  },
};
