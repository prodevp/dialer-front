import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image } from 'react-native';
import { Container, Content } from 'native-base';
import styles from './styles';
import EventsEmitter from '../../utilities/Event';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';


export default class ControlPanel extends Component {
  constructor(props) {
    super(props);
    this.state = {
      openProfile: false
    };
  }
  componentDidMount() {
    EventsEmitter.on('onRegistering', this.onRegistering);
  }

  componentWillUnmount() {
    EventsEmitter.removeListener('onRegistering', this.onRegistering);
  }

  onRegistering = (data) => {
    this.setState({ openProfile: data.data });
  }
  render() {
    const data = this.props.propsNavigation;
    const profileImgUri = 'https://antiqueruby.aliansoftware.net//Images/social/ic_msg_user01_s21_29.jpg';
    return (
      <Container style={styles.menuContainer}>
        <Content style={styles.menucontrolPanel}>

          <View style={styles.userProfiles}>
            <Image source={{ uri: profileImgUri }} style={styles.userImageStyle} />
            <View style={styles.userDetailsStyle}>
              <Text style={styles.userDetailsText}>{'John'}</Text>
              <Text style={styles.userDetailsText}>{'Seattle, USA'}</Text>
            </View>
          </View>

          <View style={styles.menumainview}>
            <TouchableOpacity onPress={() => this.state.openProfile ? data.navigation.navigate('ProfileDetail') : alert('You are not authorised to view this feature')} >
              <View style={styles.listrow}>
                <Ionicons name="md-home" color="#ffffff" size={20} />
                <Text style={styles.rowtxt}>{'Profile'}</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => data.navigation.navigate('VideoPlayer')} >
              <View style={styles.listrow}>
                <MaterialCommunityIcons name="file-outline" color="#ffffff" size={20} />
                <Text style={styles.rowtxt}>{'Video Player'}</Text>
              </View>
            </TouchableOpacity>
          </View>
        </Content>
      </Container>
    );
  }

}
