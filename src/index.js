import React from 'react';
import { View, StyleSheet } from 'react-native';
import { Provider } from 'react-redux';
import configureStore from './config/configureStore';
import Root from './Root';
import Constants from './constants';
import { PersistGate } from 'redux-persist/es/integration/react';
// import { Loader } from './components/common';

/**
 * @function: Configuring redux store.
 * */
const { store, persistor } = configureStore();

/*
 * Main component
 * */

class Application extends React.Component {
  /**
   * @function: Default render function
   * */
  render() {
    return (
      <View style={styles.container}>
        <Provider store={store}>
          <PersistGate
            persistor={persistor}
          >
            <Root />
          </PersistGate>
        </Provider>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Constants.Colors.White
  }
});

export default Application;
