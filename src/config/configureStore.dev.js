

import { compose, applyMiddleware, createStore } from 'redux';
import { persistStore } from 'redux-persist';
import { Platform } from 'react-native';
import devToolsEnhancer from 'remote-redux-devtools';
import thunk from 'redux-thunk';
import reducer from '../redux';

/*
* @function : Checks log of store.
*/

const logger = store => next => action => {
	console.log('redux action logged', action); //eslint-disable-line
	return next(action);
};

/**
 * @function: Configuring and creating redux store
 * */
export default function configureStore() {
	/**
	 * @function: Creating redux store
	 * */
	const store = createStore(
		reducer(),
		compose(
			devToolsEnhancer({
				name: Platform.OS,
				hostname: 'localhost',
				port: 5678
			})
		),
		applyMiddleware(logger, thunk)
	);

	/**
	 * @function: Persisting store for save all store's data except blacklisted reducers in device's memory
	 * */
	let persistor = persistStore(store);

	/**
	 * @return: returning store and storage persistor when it's successfully created
	 * */
	return { persistor, store };
}
