
/* eslint-disable no-unused-vars */

const localhost = '',
	  staging = '' ;

const runningUrl = process.env.NODE_ENV === 'production' ? staging : localhost,
	httpUrl = `http://${runningUrl}/`,
	socketUrl = `ws://${runningUrl}/subscriptions`,
	apiBaseUrl = `http://${runningUrl}/`,
	mediaBaseUrl = `http://${runningUrl}/api/util/file/`;

export default class Connection {
	static getBaseUrl() {
		return httpUrl;
	}

	static getResturl() {
		return apiBaseUrl;
	}

	static getSocketUrl() {
		return socketUrl;
	}

	static getMedia(fileId) {
		return mediaBaseUrl + fileId;
	}

	static getStaticContent(url) {
		return httpUrl + url;
	}
}
