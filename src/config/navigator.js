import {
  createStackNavigator,
  createDrawerNavigator,
  createSwitchNavigator
} from 'react-navigation';
import Profile from '../containers/auth/Profile';
import HamburgerMenu from '../components/Drawer';
import Loader from '../components/common/Loader';
import ProfileDetail from '../containers/auth/ProfileDetail';
import VideoPlayer from '../containers/auth/VideoPlayer';
const AuthStack = createStackNavigator({
  Loader: { screen: Loader }
});

const HomeDrawerNavigator = createDrawerNavigator(
  {
    Profile: {
      screen: Profile,
      navigationOptions: {
        header: null
      }
    },
    ProfileDetail: {
      screen: ProfileDetail,
      navigationOptions: {
        header: null
      }
    },
    VideoPlayer: {
      screen: VideoPlayer,
      navigationOptions: {
        header: null
      }
    }
  },
  {
    // drawerPosition: 'left',
    contentComponent: HamburgerMenu
  },
  {
    initialRouteName: 'Profile'
  }
);

const PrimaryNav = createSwitchNavigator(
  {
    Home: {
      screen: HomeDrawerNavigator
    },
    Auth: {
      screen: AuthStack
    }
  },
  {
    initialRouteName: 'Auth',
    navigationOptions: {
      header: null
    }
  }
);
export default PrimaryNav;
