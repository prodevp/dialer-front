import React from 'react';
import { View, StyleSheet,Text,FlatList,Image,TouchableOpacity } from 'react-native';
import Constants from '../../constants';
import NavigationBar from 'react-native-navbar';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import SearchInput, { createFilter } from 'react-native-search-filter';
const KEYS_TO_FILTERS = ['name', 'phoneNumber'];
class Profile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      dataSource: props.navigation.state.params?props.navigation.state.params.data:[],
      searchTerm: ''
    };
  }
  searchUpdated(term) {
    this.setState({ searchTerm: term });
  }
  _renderRow(item) {
		return (
			<View style={styles.mainList}>
				<View style={styles.photoContainer}>
					<Image source={{ uri: item.image }} style={styles.userImageStyle} />
				</View>
				<View style={styles.phoneContainer}>
				<Text style={styles.nameText}>{item.name}</Text>
				<TouchableOpacity activeOpacity={0.9}><Text style ={styles.phoneText}>{item.phoneNumber}</Text></TouchableOpacity>
				</View>
			</View>
		);
	}

  render() {
    const filteredContacts =  this.state.dataSource.filter(createFilter(this.state.searchTerm, KEYS_TO_FILTERS));
    const titleConfig = { title: 'Contacts Serach', tintColor: Constants.Colors.White, style: { fontSize: 18 } };
    return (
      <View style={styles.container}>
        <NavigationBar
          style={{ backgroundColor: Constants.Colors.SearchTabBar }}
					title={titleConfig}
					leftButton={<TouchableOpacity style={styles.drawerStyle} activeOpacity={0.9} onPress={() => this.props.navigation.navigate('Loader')}>
					<MaterialIcons name="chevron-left" size={40} color='white' />
			</TouchableOpacity>}
          statusBar={{ style: 'light-content', tintColor: Constants.Colors.Black }}
        />
        <SearchInput
          onChangeText={(term) => { this.searchUpdated(term);}}
          style={styles.searchInput}
          placeholder="Type number /name"
          />
          {filteredContacts.length>0?
        <FlatList
						data={filteredContacts}
						showsVerticalScrollIndicator={false}
						renderItem={({ item }) =>
							this._renderRow(item)
            }
          />:
          <View style={styles.noContactFound}>
            <Text>{'No Contact Found'}</Text>
            </View>
          }
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
		flex: 1,
		backgroundColor: Constants.Colors.White
	},
	mainList: {
		flexDirection: 'row',
		marginHorizontal:Constants.BaseStyle.DEVICE_WIDTH/100*3
	},
	noContactFound:{
		justifyContent:'center',
		alignSelf:'center',
		flex:1
	},
	photoContainer: {
		marginVertical: Constants.BaseStyle.DEVICE_WIDTH / 100 * 2,
		flex: 1
	},
	userImageStyle: {
		height: Constants.BaseStyle.DEVICE_WIDTH / 100 * 16,
		width: Constants.BaseStyle.DEVICE_WIDTH / 100 * 16,
		borderRadius: Constants.BaseStyle.DEVICE_WIDTH / 100 * 8
  },
  searchInput:{
    height: Constants.BaseStyle.DEVICE_WIDTH / 100 * 12,
		width: Constants.BaseStyle.DEVICE_WIDTH,
  	marginHorizontal:Constants.BaseStyle.DEVICE_WIDTH/100*3,
    // borderWidth: 1
  },
	phoneContainer:{
		flex: 4,
		justifyContent:'center',
		alignContent:'center',
		marginLeft:Constants.BaseStyle.DEVICE_WIDTH/100*2
	},

	nameText:{
    fontSize:16
	},
	phoneText:{
		fontSize:14,
		color:Constants.Colors.SearchTabBar
	}
});
export default Profile;
