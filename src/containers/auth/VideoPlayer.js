

import React from 'react';
import { View,TouchableOpacity,Text,StyleSheet } from 'react-native';
import Constants from '../../constants';
import NavigationBar from 'react-native-navbar';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

class Loader extends React.Component {
	render() {
        const titleConfig = { title: 'Video Player', tintColor: Constants.Colors.White, style: { fontSize: 25 } };
		return (
			<View style={styles.container}>
             <NavigationBar
                    style={{ backgroundColor: Constants.Colors.Black }}
                    title={titleConfig}
                    leftButton={<TouchableOpacity style={styles.drawerStyle} activeOpacity={0.9} onPress={() => this.props.navigation.navigate('Profile')}>
                        <MaterialIcons name="chevron-left" size={40} color='white' />
                    </TouchableOpacity>}
                    statusBar={{ style: 'light-content', tintColor: Constants.Colors.Black }}
                />
                <View style={styles.mainContainer}>
				<TouchableOpacity onPress={()=>this.props.navigation.navigate('Profile')}>
					<Text style={styles.text}> {'Video Player'}</Text>
				</TouchableOpacity>
                </View>
			</View>
			);

	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
        backgroundColor:Constants.Colors.White
	},
	text:{
		fontSize:25
    },
    mainContainer:{
        justifyContent:'center',
        alignItems:'center',
        flex:1
    }
});
module.exports = Loader;
