import React from 'react';
import { View, StyleSheet, Text, TouchableOpacity, Image } from 'react-native';
import Constants from '../../constants';
import NavigationBar from 'react-native-navbar';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
class ProfileDetail extends React.Component {
    render() {
        const profileImgUri = 'https://antiqueruby.aliansoftware.net//Images/social/ic_msg_user01_s21_29.jpg';
        const titleConfig = { title: 'ProfileDetail', tintColor: Constants.Colors.White, style: { fontSize: 25 } };
        return (
            <View style={styles.container}>
                <NavigationBar
                    style={{ backgroundColor: Constants.Colors.Black }}
                    title={titleConfig}
                    leftButton={<TouchableOpacity style={styles.drawerStyle} activeOpacity={0.9} onPress={() => this.props.navigation.navigate('Profile',{props:true})}>
                        <MaterialIcons name="chevron-left" size={40} color='white' />
                    </TouchableOpacity>}
                    statusBar={{ style: 'light-content', tintColor: Constants.Colors.Black }}
                />
                <View style={styles.mainContainer}>
                    <View style={styles.imageContainer}>
                        <Image source={{ uri: profileImgUri }} style={styles.userImageStyle} />
                    </View>
                    <View style={styles.detailContainer}>
                        <Text style={styles.textStyle}>{'John Smith '} </Text>
                        <Text style={styles.textStyle}>{'Berlin , Germany '} </Text>
                        <Text style={styles.textStyle}>{'+654337887489'} </Text>
                    </View>
                </View>

            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Constants.Colors.White
    },
    textStyle: {
        color: Constants.Colors.Gray,
        paddingVertical: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 0.23
    },
    detailContainer: {
        marginLeft: Constants.BaseStyle.DEVICE_WIDTH / 100 * 5
    },
    mainContainer: {
        flexDirection: 'row',
        margin: Constants.BaseStyle.DEVICE_WIDTH / 100 * 5
    },
    drawerStyle: {
        justifyContent: 'center'
    },
    userImageStyle: {
        height: Constants.BaseStyle.DEVICE_WIDTH / 100 * 18,
        width: Constants.BaseStyle.DEVICE_WIDTH / 100 * 18,
        borderRadius: Constants.BaseStyle.DEVICE_WIDTH / 100 * 9
    }
});
export default ProfileDetail;
