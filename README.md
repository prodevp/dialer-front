Set-up :
In order to configure your device as per the react-native development environment, follow https://facebook.github.io/react-native/docs/getting-started.html

Once set-up is done, run the following command in the root-directory of project in the terminal:
 react-native run-ios
